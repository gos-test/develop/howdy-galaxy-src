#!/bin/sh

currentPath=`dirname -- "$(readlink -f -- "$0"; )"`
srcPath=$currentPath/../src

# build pam lib:

cd $currentPath
rm -rf pamBuild
mkdir pamBuild
cd pamBuild

qmake-qt5 $srcPath/pam/pam_howdy.pro
if [ $? -ne 0 ]; then
    echo qmake failed
    exit 1
fi

make -j4
if [ $? -ne 0 ]; then
    echo make failed
    exit 2
fi

echo Howdy pam module builded
exit 0
