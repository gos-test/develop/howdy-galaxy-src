#!/bin/sh

currentPath=`dirname -- "$(readlink -f -- "$0"; )"`
srcPath=$currentPath/../src
packagePath=$currentPath/howdy-mts

# create target dir:

cd $currentPath
rm -rf $packagePath
mkdir $packagePath

# build pam lib:

cd $currentPath
rm -rf pamBuild
mkdir pamBuild
cd pamBuild

qmake-qt5 $srcPath/pam/pam_howdy.pro
if [ $? -ne 0 ]; then
    echo qmake failed
    exit 1
fi

make -j4
if [ $? -ne 0 ]; then
    echo make failed
    exit 2
fi

# copy target files:

checkCopy() {
    if [ $? -ne 0 ]; then
        echo copy failed
        exit 3
    fi
}

cd $currentPath

mkdir -p $packagePath/etc/howdy
cp $srcPath/config.ini.sample $packagePath/etc/howdy/
checkCopy

mkdir -p $packagePath/etc/howdy/dlib-data
cp $srcPath/dlib-data/*.dat $packagePath/etc/howdy/dlib-data/
checkCopy

mkdir -p $packagePath/etc/pam.d
cp $srcPath/pam/pam_howdy $packagePath/etc/pam.d/
checkCopy

mkdir -p $packagePath/usr/etc/howdy
cp $srcPath/*.py $packagePath/usr/etc/howdy/
checkCopy

mkdir -p $packagePath/usr/etc/howdy/cli
cp $srcPath/cli/*.py $packagePath/usr/etc/howdy/cli/
checkCopy

mkdir -p $packagePath/usr/etc/howdy/recorders
cp $srcPath/recorders/*.py $packagePath/usr/etc/howdy/recorders/
checkCopy

mkdir -p $packagePath/usr/etc/howdy/rubberstamps
cp $srcPath/rubberstamps/*.py $packagePath/usr/etc/howdy/rubberstamps/
checkCopy

mkdir -p $packagePath/usr/lib
cp $currentPath/pamBuild/libpam_howdy.so.1.0.0 $packagePath/usr/lib/pam_howdy.so
checkCopy

# create pkg:

cd $packagePath
find . -type f | grep -v ./plist | sed 's/^.//' > plist

cd $currentPath
cp ./+MANIFEST $packagePath/
cp ./+POST_INSTALL $packagePath
cp ./+PRE_DEINSTALL $packagePath
cp ./+PRE_INSTALL $packagePath
pkg create -m $packagePath/ -r $packagePath/ -p $packagePath/plist -v -o .
