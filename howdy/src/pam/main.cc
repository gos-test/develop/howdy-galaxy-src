#include <cerrno>
#include <csignal>
#include <cstdlib>
#include <ostream>

#include <glob.h>
#include <libintl.h>
#include <pthread.h>
#include <spawn.h>
#include <stdexcept>
#include <signal.h>
#include <sys/syslog.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <syslog.h>
#include <unistd.h>

#include <chrono>
#include <cstring>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <memory>
#include <string>
#include <system_error>
#include <tuple>
#include <vector>
#include <filesystem>

#include <INIReader.h>

#include <security/pam_appl.h>
#include <security/openpam.h>
#include <security/pam_modules.h>

#include "main.hh"

const auto PYTHON_EXECUTABLE = "python3.10";
const auto COMPARE_PROCESS_PATH = "/usr/local/share/howdy/compare.py";

#define S(msg) msg

/**
 * Inspect the status code returned by the compare process
 * @param  status        The status code
 * @param  conv_function The PAM conversation function
 * @return               A PAM return code
 */
auto howdy_error(int status,
                 const std::function<int(int, const char *)> &conv_function)
    -> int {
  // If the process has exited
  if (WIFEXITED(status)) {
    // Get the status code returned
    status = WEXITSTATUS(status);

    switch (status) {
    case CompareError::NO_FACE_MODEL:
      syslog(LOG_NOTICE, "Failure, no face model known");
      break;
    case CompareError::TIMEOUT_REACHED:
      syslog(LOG_ERR, "Failure, timeout reached");
      break;
    case CompareError::ABORT:
      syslog(LOG_ERR, "Failure, general abort");
      break;
    case CompareError::TOO_DARK:
      conv_function(PAM_ERROR_MSG, S("Face detection image too dark"));
      syslog(LOG_ERR, "Failure, image too dark");
      break;
    case CompareError::INVALID_DEVICE:
      syslog(LOG_ERR, "Failure, not possible to open camera at configured path");
      break;
    default:
      syslog(LOG_ERR, "Failure, unknown error %d", status);
    }
  } else if (WIFSIGNALED(status)) {
    // We get the signal
    status = WTERMSIG(status);

    syslog(LOG_ERR, "Child killed by signal %s (%d)", strsignal(status),
           status);
  }

  if (conv_function(PAM_ERROR_MSG, S("Лицо не распознано. Повторите попытку")) != PAM_SUCCESS) {
    syslog(LOG_ERR, "Failed to send detection notice result");
  } else {
    syslog(LOG_ERR, "PAM_ERROR_MSG success. status: %d", status);
  }


  // As this function is only called for error status codes, signal an error to
  // PAM
  return PAM_AUTH_ERR;
}

/**
 * Format the success message if the status is successful or log the error in
 * the other case
 * @param  status        Status code
 * @param  conv_function PAM conversation function
 * @return          Returns the conversation function return code
 */
auto howdy_status(int status, const std::function<int(int, const char *)> &conv_function)
    -> int {
  if (status != EXIT_SUCCESS) {
    return howdy_error(status, conv_function);
  }

  syslog(LOG_INFO, "Login approved");

  return PAM_SUCCESS;
}

/**
 * Check if Howdy should be enabled according to the configuration and the
 * environment.
 * @param  config INI configuration
 * @return        Returns PAM_AUTHINFO_UNAVAIL if it shouldn't be enabled,
 * PAM_SUCCESS otherwise
 */
auto check_enabled(const INIReader &config) -> int {
  // Stop executing if Howdy has been disabled in the config
  if (config.GetBoolean("core", "disabled", false)) {
    syslog(LOG_INFO, "Skipped authentication, Howdy is disabled");
    return PAM_AUTHINFO_UNAVAIL;
  }

  // Stop if we're in a remote shell and configured to exit
  if (config.GetBoolean("core", "abort_if_ssh", true)) {
    if (getenv("SSH_CONNECTION") != nullptr ||
        getenv("SSH_CLIENT") != nullptr || getenv("SSHD_OPTS") != nullptr) {
      syslog(LOG_INFO, "Skipped authentication, SSH session detected");
      return PAM_AUTHINFO_UNAVAIL;
    }
  }

  // Try to detect the laptop lid state and stop if it's closed
  if (config.GetBoolean("core", "abort_if_lid_closed", true)) {
    glob_t glob_result;

    // Get any files containing lid state
    int return_value =
        glob("/proc/acpi/button/lid/*/state", 0, nullptr, &glob_result);

    if (return_value != 0) {
      syslog(LOG_ERR, "Failed to read files from glob: %d", return_value);
      if (errno != 0) {
        syslog(LOG_ERR, "Underlying error: %s (%d)", strerror(errno), errno);
      }
    } else {
      for (size_t i = 0; i < glob_result.gl_pathc; i++) {
        std::ifstream file(std::string(glob_result.gl_pathv[i]));
        std::string lid_state;
        std::getline(file, lid_state, static_cast<char>(file.eof()));

        if (lid_state.find("closed") != std::string::npos) {
          globfree(&glob_result);

          syslog(LOG_INFO, "Skipped authentication, closed lid detected");
          return PAM_AUTHINFO_UNAVAIL;
        }
      }
    }
    globfree(&glob_result);
  }

  return PAM_SUCCESS;
}

/**
 * The main function, runs the identification and authentication
 * @param  pamh     The handle to interface directly with PAM
 * @param  flags    Flags passed on to us by PAM, XORed
 * @param  argc     Amount of rules in the PAM config (disregared)
 * @param  argv     Options defined in the PAM config
 * @param  auth_tok True if we should ask for a password too
 * @return          Returns a PAM return code
 */
auto identify(pam_handle_t *pamh, int /*flags*/, int argc, const char **argv,
              bool auth_tok) -> int {
  INIReader config("/etc/howdy/config.ini");
  openlog("pam_howdy", 0, LOG_AUTHPRIV);

  syslog(LOG_INFO, "howdy start, auth_tok %d", auth_tok);

  // Error out if we could not read the config file
  if (config.ParseError() != 0) {
    syslog(LOG_ERR, "Failed to parse the configuration file: %d",
           config.ParseError());
    return PAM_SYSTEM_ERR;
  }

  // Will contain the responses from PAM functions
  int pam_res = PAM_IGNORE;

  // Check if we should continue
  if ((pam_res = check_enabled(config)) != PAM_SUCCESS) {
    return pam_res;
  }

  // Get the username from PAM, needed to match correct face model
  char *username = nullptr;
  if ((pam_res = pam_get_user(pamh, const_cast<const char **>(&username),
                              nullptr)) != PAM_SUCCESS) {
    syslog(LOG_ERR, "Failed to get username");
    return pam_res;
  }

  // check face model for user
  if (std::filesystem::exists(std::string("/etc/howdy/models/") + username + ".dat") == false) {
    syslog(LOG_INFO, "No face model for user %s", username);
    return PAM_IGNORE;
  }

  // we need to known is it a console login or from kde
  char *pamTty = nullptr;
  pam_get_item(pamh, PAM_TTY, (const void**)&pamTty);
  if (pamTty) {
    syslog(LOG_INFO, "pam_tty: %s", pamTty);
  }

  enum class TtyType { Unknown, None, Console, Display };
  TtyType ttyType = TtyType::Unknown;
  if (pamTty == nullptr) {        // typically for login screen
    ttyType = TtyType::None;
  } else if (pamTty[0] == ':') {  // :0 for example, typically for lock screen
    ttyType = TtyType::Display;
  } else {
    ttyType = TtyType::Console; // /dev/tty0 or /dev/pts/1
  }

  // check parameters
  bool faceOnly = false;
  for (int i = 0; i < argc; ++i) {
    if (strcmp(argv[i], "face_only") == 0) {
      faceOnly = true;
      break;
    }
  }
  syslog(LOG_INFO, "face_only flag is %s", faceOnly ? "set" : "not set");

  if (faceOnly) {
    syslog(LOG_INFO, "Using force face recognition for login");
  } else {
    // if login in desktop enviroment or on lock screen
    if (ttyType == TtyType::None || ttyType == TtyType::Display) {
      char *auth_tok_ptr = nullptr;
      if (PAM_SUCCESS == pam_get_authtok(pamh, PAM_AUTHTOK, const_cast<const char **>(&auth_tok_ptr), nullptr)) {
        if (auth_tok_ptr && std::string(auth_tok_ptr).empty() == false) {
          syslog(LOG_INFO, "Using password for login");
          return PAM_IGNORE;
        } else {
            syslog(LOG_INFO, "Using face recognition for login");
        }
      } else {
          syslog(LOG_INFO, "pam_get_authtok failed");
      }
    }
  }

  // Will contain PAM conversation structure
  struct pam_conv *conv = nullptr;
  const void **conv_ptr =
      const_cast<const void **>(reinterpret_cast<void **>(&conv));

  if ((pam_res = pam_get_item(pamh, PAM_CONV, conv_ptr)) != PAM_SUCCESS) {
    syslog(LOG_ERR, "Failed to acquire conversation");
    return pam_res;
  }
  if (!conv) {
    syslog(LOG_ERR, "No pam_conv");
  }

  // Wrap the PAM conversation function in our own, easier function
  auto conv_function = [conv](int msg_type, const char *msg_str) {
    const struct pam_message msg = {.msg_style = msg_type, .msg = (char*)msg_str};
    const struct pam_message *msgp = &msg;

    struct pam_response res = {};
    struct pam_response *resp = &res;

    return conv ? conv->conv(1, &msgp, &resp, conv->appdata_ptr) : PAM_BAD_HANDLE;
  };

  syslog(LOG_INFO, "starting face recognition for user %s", username);

  // If enabled, send a notice to the user that facial login is being attempted
  if (config.GetBoolean("core", "detection_notice", false)) {
    // it's important to use conv_function only in console
    if (ttyType == TtyType::Console) {
      if ((conv_function(PAM_TEXT_INFO, S("Attempting facial authentication"))) != PAM_SUCCESS) {
          syslog(LOG_ERR, "Failed to send detection notice");
      }
    }
  }

  const char *const args[] = {PYTHON_EXECUTABLE, // NOLINT
                              COMPARE_PROCESS_PATH, username, nullptr};
  pid_t child_pid;

  // Start the python subprocess
  if (posix_spawnp(&child_pid, PYTHON_EXECUTABLE, nullptr, nullptr,
                   const_cast<char *const *>(args), nullptr) != 0) {
    syslog(LOG_ERR, "Can't spawn the howdy process: %s (%d)", strerror(errno),
           errno);
    return PAM_SYSTEM_ERR;
  }

  syslog(LOG_INFO, "Python compare started");

  int status;
  wait(&status);

  // If python process ran into a timeout
  // Do not send enter presses or terminate the PAM function, as the user might still be typing their password
  if (WEXITSTATUS(status) == CompareError::TIMEOUT_REACHED && WIFEXITED(status)) {
    if (faceOnly) {
      syslog(LOG_INFO, "Face not recognized. Timeout reached. face_only flag is set - return PAM_AUTH_ERR");
      return PAM_AUTH_ERR;
    } else {
      if (ttyType == TtyType::Console) {
          syslog(LOG_INFO, "Face not recognized. Timeout reached. Console tty - return PAM_IGNORE");
          if (conv_function(PAM_ERROR_MSG, S("Face is not recognized")) != PAM_SUCCESS) {
            syslog(LOG_ERR, "Failed to send detection notice result");
          }
          return PAM_IGNORE;
      } else if (ttyType == TtyType::None || ttyType == TtyType::Display) {
          if (conv_function(PAM_ERROR_MSG, "Лицо не распознано. Повторите попытку") != PAM_SUCCESS) {
            syslog(LOG_ERR, "Failed to send detection notice result");
          } else {
            syslog(LOG_ERR, "PAM_ERROR_MSG success. status: %d", WEXITSTATUS(status));
          }

          syslog(LOG_INFO, "Face not recognized. Timeout reached. DE tty - return PAM_AUTH_ERR");
          return PAM_AUTH_ERR;
      }
    }

    syslog(LOG_INFO, "Face not recognized. Timeout reached. Unknown tty - return PAM_IGNORE");
    // The password has been entered, we are passing it to PAM stack
    return PAM_IGNORE;
  }

  return howdy_status(status, conv_function);
}

// Called by PAM when a user needs to be authenticated, for example by running
// the sudo command
PAM_EXTERN auto pam_sm_authenticate(pam_handle_t *pamh, int flags, int argc,
                                    const char **argv) -> int {
  return identify(pamh, flags, argc, argv, true);
}

// Called by PAM when a session is started, such as by the su command
PAM_EXTERN auto pam_sm_open_session(pam_handle_t *pamh, int flags, int argc,
                                    const char **argv) -> int {
  return identify(pamh, flags, argc, argv, false);
}

// The functions below are required by PAM, but not needed in this module
PAM_EXTERN auto pam_sm_acct_mgmt(pam_handle_t */*pamh*/, int /*flags*/, int /*argc*/,
                                 const char **/*argv*/) -> int {
  return PAM_IGNORE;
}
PAM_EXTERN auto pam_sm_close_session(pam_handle_t */*pamh*/, int /*flags*/, int /*argc*/,
                                     const char **/*argv*/) -> int {
  return PAM_IGNORE;
}
PAM_EXTERN auto pam_sm_chauthtok(pam_handle_t */*pamh*/, int /*flags*/, int /*argc*/,
                                 const char **/*argv*/) -> int {
  return PAM_IGNORE;
}
PAM_EXTERN auto pam_sm_setcred(pam_handle_t */*pamh*/, int /*flags*/, int /*argc*/,
                               const char **/*argv*/) -> int {
  return PAM_IGNORE;
}
