CONFIG -= qt app_bundle
CONFIG += c++17
CONFIG += console

TEMPLATE = lib
TARGET = pam_howdy

HEADERS += \
    main.hh \

SOURCES += \
    main.cc \

INCLUDEPATH += \
    /usr/local/include/

LIBS += -linih -lpam -lINIReader

QMAKE_POST_LINK = cp -f $$_PRO_FILE_PWD_/install.sh $$OUT_PWD/; chmod a+rwx $$OUT_PWD/install.sh; cp -f $$_PRO_FILE_PWD_/pam_howdy $$OUT_PWD/;
