#!/bin/sh

cp -f libpam_howdy.so.1.0.0 /usr/lib/pam_howdy.so
chown root:wheel /usr/lib/pam_howdy.so

cp -f pam_howdy /etc/pam.d/
chown root:wheel /etc/pam.d/pam_howdy
chmod 644 /etc/pam.d/pam_howdy
