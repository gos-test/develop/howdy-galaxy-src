#ifndef MAIN_H_
#define MAIN_H_

// Exit status codes returned by the compare process
enum CompareError : int {
  NO_FACE_MODEL = 10,
  TIMEOUT_REACHED = 11,
  ABORT = 12,
  TOO_DARK = 13,
  INVALID_DEVICE = 14,
  RUBBERSTAMP = 15
};

#endif // MAIN_H_
